﻿using System;

namespace Sandbox.Entities
{
    public class PersonDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonType { get; set; }
        public string Brn { get; set; }
        public string EmployeeNumber { get; set; }
        public string MyProperty { get; set; }
    }
}
