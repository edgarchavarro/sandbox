﻿if ($("#TestPage").length > 0) {

    //new Vue({
    //    el: '#tabs',
    //    data: { activetab: 1 }
    //});
    

    var test = new Vue({
        el: '#TestPage',
        data: {
            errors: [],
            person: {
                firstName: '',
                lastName: '',
                personType: ''
            }
        },
        methods: {
            checkFields: function () {
                var vm = this;

                if (vm.person.firstName === '') {
                    vm.errors.push("Name is required.");
                }
                else {
                    vm.gretting();
                }
            },
            gretting: function () {

                var vm = this;
                var data = {
                    personDTO: {
                        FirstName: vm.person.firstName,
                        LastName: vm.person.lastName,
                        personType: vm.person.personType
                    }
                };

                var route = '/Home/CreateUser';
                               
                jQuery.ajax({
                    type: "POST",
                    url: route,
                    data: data,
                    dataType: 'json',
                    beforeSend: function () {
                        console.log(data);
                    },
                    success: function (resp) {

                        alert(resp);
                        console.log(resp);

                    },
                    complete: function () {
                    },
                    error: function (e) {
                        console.log('Error: ' + e);
                    }
                }); 

            }
        },
        mounted: function () {
            
        }
    });
}
