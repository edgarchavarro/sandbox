﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using SandbowMVC.Models;
using Sandbox.Entities;


namespace SandbowMVC.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult TestPage()
        {
            ViewData["Message"] = "Test page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpPost]
        public JsonResult CreateUser(PersonDTO personDTO)
        {
            Sandbox.Services.PersonService obj = new Sandbox.Services.PersonService(personDTO);
            
            return Json(obj.Create());

        }
    }
}
