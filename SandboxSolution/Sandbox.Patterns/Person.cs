﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandbox.Patterns
{
    abstract class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public abstract string CreateUser();
    }

    //class Patient : Person
    //{
    //    public string Brn { get; set; }

    //    public override string CreateUser()
    //    {
    //        return ($"Patient {FirstName} {LastName} was created.");
    //    }

    //}

    //class Visitor : Person
    //{
    //    public string Address { get; set; }
    //    public int PhoneNumber { get; set; }

    //    public override string CreateUser()
    //    {
    //        return ($"Visitor : {FirstName} {LastName} was created.");
    //    }
    //}

    //class Staff : Person
    //{
    //    public int EmployeeNumber { get; set; }

    //    public override string CreateUser()
    //    {
    //        return ($"Staff {FirstName} {LastName} was created");
    //    }
    //}

    //class ExternalStaff : Person
    //{
    //    public string Password { get; set; }

    //    public override string CreateUser()
    //    {
    //        return ($"External Staff {FirstName} {LastName} was created.");
    //    }
    //}
    //abstract class Creator
    //{
    //    public abstract Person FactoryMethod();
    //}

    //class ConcretePatient : Creator
    //{
    //    public string Brn { get; set; }

    //    public override Person FactoryMethod()
    //    {
    //        return new Patient() { Brn = Brn };
    //    }

    //}

    //class ConcreteVisitor : Creator
    //{
    //    private string _address;
    //    private int _phoneNumber;

    //    public int MyProperty { get; set; }
    //    public override Person FactoryMethod()
    //    {
    //        return new Visitor() { Address = _address, PhoneNumber = _phoneNumber };
    //    }

    //    public ConcreteVisitor()
    //    {

    //    }

    //    public ConcreteVisitor(string address, int phoneNumber) : this()
    //    {
    //        _address = address;
    //        _phoneNumber = phoneNumber;
    //    }

    //    public ConcreteVisitor(string address) : this(address, 0)
    //    {

    //    }

    //    public ConcreteVisitor(int phoneNumber) : this(null, phoneNumber)
    //    {

    //    }
    //}

    //class ConcreteStaff : Creator
    //{
    //    public int EmployeeNumber { get; set; }
    //    public override Person FactoryMethod()
    //    {
    //        return new Staff() { EmployeeNumber = EmployeeNumber };
    //    }
    //}

    //class ConcreteExternalStaff : Creator
    //{
    //    public string Password { get; set; }

    //    public override Person FactoryMethod()
    //    {
    //        return new ExternalStaff() { Password = Password };
    //    }
    //}
}
