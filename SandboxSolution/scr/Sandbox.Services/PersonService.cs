﻿using System;
using Sandbox.Entities;

namespace Sandbox.Services
{
    enum PersonType
    {
        Visitor = 1,
        Patient = 2,
        Staff = 3
    }
    public class PersonService
    {
        protected PersonDTO _person;
        public PersonService(PersonDTO person)
        {
            _person = person;
        }

        public string Create() {

            string result = "Person not created";
            Sandbox.Patterns.Creator creator;
            Sandbox.Patterns.Person person;

            if (_person.PersonType == 1)
            {
                creator = new Sandbox.Patterns.ConcreteVisitor("Oxford Street", 123456);
                person = creator.FactoryMethod();
                person.FirstName = _person.FirstName;
                person.LastName = _person.LastName;
                
                result = person.CreatePerson();
            }
                      
            return result;
        }
    }
}
