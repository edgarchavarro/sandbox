﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandbox.Patterns
{
    class Patient : Person
    {
        public string Brn { get; set; }
        public override string CreatePerson()
        {
            return ($"Patient {FirstName} {LastName} {Environment.NewLine}BRN : {Brn}");
        }
    }
}
