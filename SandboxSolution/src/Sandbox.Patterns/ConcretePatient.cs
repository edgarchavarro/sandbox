﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandbox.Patterns
{
    public class ConcretePatient : Creator
    {
        public string Brn { get; set; }
        public override Person FactoryMethod()
        {
            return new Patient() { Brn = Brn };
        }
    }
}
