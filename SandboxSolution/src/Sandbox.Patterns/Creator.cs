﻿using System;

namespace Sandbox.Patterns
{
    public abstract class Creator
    {
        public abstract Person FactoryMethod();
    }
}
