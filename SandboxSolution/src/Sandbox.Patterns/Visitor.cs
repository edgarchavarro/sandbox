﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandbox.Patterns
{
    class Visitor : Person
    {
        public string Address { get; set; }
        public int PhoneNumber { get; set; }
        public override string CreatePerson()
        {
            return ($"Visitor : {FirstName} {LastName} - Address : {Address} - Phone : { PhoneNumber }");
        }
    }
}
