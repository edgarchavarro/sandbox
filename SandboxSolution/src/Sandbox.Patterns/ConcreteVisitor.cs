﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sandbox.Patterns
{
    public class ConcreteVisitor : Creator
    {
        protected string _address;
        protected int _phoneNumber;

        public int MyProperty { get; set; }
        public override Person FactoryMethod()
        {
            return new Visitor() { Address = _address, PhoneNumber = _phoneNumber };
        }

        public ConcreteVisitor()
        {

        }

        public ConcreteVisitor(string address, int phoneNumber) : this()
        {
            _address = address;
            _phoneNumber = phoneNumber;
        }

        public ConcreteVisitor(string address) : this(address, 0)
        {

        }

        public ConcreteVisitor(int phoneNumber) : this(null, phoneNumber)
        {

        }
    }
}
